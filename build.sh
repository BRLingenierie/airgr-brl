#!/bin/bash
sudo apt-get install curl

currentDir=$(pwd)
folderName=$(basename $(pwd))

cd src
for file in $(find ${PWD} -mindepth 1 -maxdepth 1 -type f -name "*.f" ) ;do   
	gfortran -fPIC -c -O3 $file
done
ar cr gr_fortran.lib frun_gr1a.o frun_gr2m.o frun_cemaneige.o frun_gr4h.o frun_gr4j.o frun_gr6j.o utils_D.o utils_H.o frun_gr5j.o
gcc -c -O3 airGR.c -I "/usr/local/lib/R/include"
gcc -o airGR2.so -shared airGR.o gr_fortran.lib /usr/local/lib/R/lib/libR.so

cd $currentDir

export VERSION=$(cat DESCRIPTION | grep Version |  sed  's/Version: //g')
export PACKAGE=$(cat DESCRIPTION | grep Package: |  sed  's/Package: //g')

cd ../

tar -czvf ${PACKAGE}_${VERSION}.tar.gz $folderName

curl -v --upload-file ${PACKAGE}_${VERSION}.tar.gz https://nexus.wimes.fr/repository/RRelease/r-hosted/src/contrib/${PACKAGE}_${VERSION}.tar.gz
